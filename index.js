const express = require('express')
const bodyParser = require('body-parser')
const { errors } = require('celebrate')
const cors = require('cors')
const boom = require('express-boom')
const morgan = require('morgan')

const config = require('./config')

const app = express()

app.use(cors())
app.use(boom())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(morgan('dev'))

require('./app/v1')(app)
require('./app/v2')(app)

app.use(errors())

app.listen(config.port, () => {
  console.log(`Server running on port ${config.port}`)
})
