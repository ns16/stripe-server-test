const axios = require('axios')
const btoa = require('btoa')

const config = require('../config')

const authorization = `Basic ${btoa(`${config.account_name}:${config.account_token}`)}`

module.exports = axios.create({
  baseURL: config.license_server_url,
  headers: {
    authorization
  }
})
