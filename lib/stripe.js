const Stripe = require('stripe')

const config = require('../config')

module.exports = new Stripe(config.stripe_secret_key)
