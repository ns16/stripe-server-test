const { to } = require('await-to-js')
const axios = require('axios')
const { celebrate, Joi, Segments } = require('celebrate')
const Stripe = require('stripe')

const db = require('../../lib/db')
const config = require('../../config')

const http = axios.create({ baseURL: config.license_server_url })

const validator = {
  [Segments.BODY]: {
    email: Joi.string().email().required(),
    source: Joi.string().required(),
    amount: Joi.number().positive().min(0.50).required(), // согласно документации Stripe API, минимальная сумма платежа должна составлять 50 центов/евроцентов, см. https://stripe.com/docs/currencies#minimum-and-maximum-charge-amounts
    currency: Joi.string().valid('usd', 'eur').required(),
    key: Joi.string().alphanum().length(32).required()
  }
}

module.exports = app => {
  app.post('/api/v2/payments', celebrate(validator), async (req, res) => {
    let customer = db.get('customers')
      .find({ email: req.body.email })
      .value()

    const [error1, res1] = await to(http.get(`/api/v1/invoices/${req.body.key}/for_server`))
    if (error1) {
      console.error(error1.response.data)
      return res.boom.badImplementation(null, { message: "Invoice with provided key doesn't exist" })
    }
    if (res1.data.data.status === 'paid') {
      return res.boom.badImplementation(null, { message: 'Invoice with provided key already is paid' })
    }
    if (res1.data.data.private_key === null) {
      return res.boom.badImplementation(null, { message: "Account doesn't use payment system" })
    }

    const stripe = new Stripe(res1.data.data.private_key)

    if (!customer) {
      const [error2, customer1] = await to(stripe.customers.create({
        email: req.body.email,
        source: req.body.source
      }))
      if (error2) {
        console.error(error2)
        return res.boom.badImplementation(null, { message: 'Stripe customer creation failed' })
      }
      db.get('customers')
        .push({
          id: customer1.id,
          email: customer1.email
        })
        .write()
      customer = customer1
    }

    const [error3] = await to(stripe.charges.create({
      amount: req.body.amount * 100, // преобразуем доллары/евро в центы/евроценты
      currency: req.body.currency,
      customer: customer.id,
    }))
    if (error3) {
      console.error(error3)
      return res.boom.badImplementation(null, { message: 'Stripe charge creation failed' })
    }

    const [error4] = await to(http.put(`/api/v1/invoices/${req.body.key}/pay`))
    if (error4) {
      console.error(error4.response.data)
      return res.boom.badImplementation(null, { message: 'Failed to change invoice status to paid' })
    }

    res.status(201).json({})
  })
}
