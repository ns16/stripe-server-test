const { to } = require('await-to-js')
const { celebrate, Joi, Segments } = require('celebrate')

const db = require('../../lib/db')
const http = require('../../lib/http')
const stripe = require('../../lib/stripe')

const validator = {
  [Segments.BODY]: {
    email: Joi.string().email().required(),
    source: Joi.string().required(),
    product_key: Joi.string().length(16).required(),
    amount: Joi.number().positive().min(0.50).required(), // согласно документации Stripe API, минимальная сумма платежа должна составлять 50 центов/евроцентов, см. https://stripe.com/docs/currencies#minimum-and-maximum-charge-amounts
    type: Joi.string().valid('perpetual', 'subscription').required(),
    expire_date: Joi.when('type', { is: 'subscription', then: Joi.when('subscription_period', { is: Joi.exist(), then: Joi.date(), otherwise: Joi.date().required() }), otherwise: Joi.date() }),
    subscription_period: Joi.string().valid('day', 'month', 'year')
  }
}

module.exports = app => {
  app.post('/api/v1/payments', celebrate(validator), async (req, res) => {
    let customer = db.get('customers')
      .find({ email: req.body.email })
      .value()

    if (!customer) {
      const [error1, customer1] = await to(stripe.customers.create({
        email: req.body.email,
        source: req.body.source
      }))
      if (error1) {
        console.error(error1)
        return res.boom.badImplementation(null, { message: 'Stripe customer creation failed' })
      }
      db.get('customers')
        .push({
          id: customer1.id,
          email: customer1.email
        })
        .write()
      customer = customer1
    }

    const [error2, res1] = await to(http.post('/api/v2/licenses/create', {
      email: req.body.email,
      stripe_id: customer.id,
      product_key: req.body.product_key,
      type: req.body.type,
      expire_date: req.body.expire_date,
      subscription_period: req.body.subscription_period
    }))
    if (error2) {
      console.error(error2.response.data)
      return res.boom.badImplementation(null, { message: 'License creation failed' })
    }

    const [error3, charge] = await to(stripe.charges.create({
      amount: req.body.amount * 100, // преобразуем доллары в центы
      currency: 'usd',
      customer: customer.id,
    }))
    if (error3) {
      console.error(error3)
      return res.boom.badImplementation(null, { message: 'Stripe charge creation failed' })
    }

    const [error4] = await to(http.post('/api/v2/licenses/register', {
      key: res1.data.data.key,
      license_ref: `${req.body.email}_${req.body.product_key}`,
      external_id: charge.id
    }))
    if (error4) {
      console.error(error4.response.data)
      return res.boom.badImplementation(null, { message: 'License created, but registration failed' })
    }

    res.status(201).json({ data: res1.data.data })
  })
}
